"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_br_1 = require("auth-br");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class JWT extends auth_br_1.TokenManager {
    constructor(secret, expire) {
        super();
        this.secret = secret;
        this.expire = expire;
    }
    async sign(account) {
        try {
            const expire = Math.floor(Date.now() / 1000) + (this.expire);
            const token = jsonwebtoken_1.default.sign({
                exp: expire,
                sub: account.id
            }, this.secret);
            return { token, expire };
        }
        catch (e) {
            return null;
        }
    }
    async verify(bearer) {
        try {
            return jsonwebtoken_1.default.verify(bearer, this.secret);
        }
        catch (e) {
            return false;
        }
    }
}
exports.default = JWT;
