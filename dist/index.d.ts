import { Token, TokenManager } from 'auth-br';
declare class JWT extends TokenManager {
    private secret;
    private expire;
    constructor(secret: string, expire: number);
    sign(account: Account): Promise<Token>;
    verify(bearer: string): Promise<boolean | any>;
}
export default JWT;
