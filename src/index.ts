import { Token, TokenManager } from 'auth-br'
import jsonwebtoken from 'jsonwebtoken'

class JWT extends TokenManager {
  private secret: string
  private expire: number

  constructor (secret: string, expire: number) {
    super()
    this.secret = secret
    this.expire = expire
  }

  async sign (account: Account): Promise<Token> {
    try {
      const expire = Math.floor(Date.now() / 1000) + (this.expire)
      const token = jsonwebtoken.sign(
        {
          exp: expire,
          sub: account.id
        },
        this.secret
      )
      return { token, expire }
    } catch (e) {
      return null
    }
  }

  async verify (bearer: string): Promise<boolean | any> {
    try {
      return jsonwebtoken.verify(bearer, this.secret)
    } catch (e) {
      return false
    }
  }
}
export default JWT
